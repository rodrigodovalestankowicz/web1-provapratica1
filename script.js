let textElement = document.querySelector("textarea");
let previewElement = document.querySelector("#preview");

textElement.addEventListener("input", () => {
    const textValue = textElement.value;
    previewElement.innerHTML = textValue;
});

let fontSizeElement = document.querySelector("#fontSize");
fontSizeElement.addEventListener("input", () => {
    const fontSize = fontSizeElement.value;
    previewElement.style.fontSize = `${fontSize}px`;
});

let fontFamilyElement = document.querySelector("#fontFamily");
fontFamilyElement.addEventListener("input", () => {
    const fontFamily = fontFamilyElement.value;
    previewElement.style.fontFamily = fontFamily;
});

let colorElement = document.querySelector("#color");
colorElement.addEventListener("input", () => {
    const color = colorElement.value;
    previewElement.style.color = color;
});

let speedElement = document.querySelector("#speed");
speedElement.addEventListener("input", () => {
    const speed = speedElement.value;
});

$(document).ready(function () {
    $("#textArea").trigger("input");
    fontSizeElement.dispatchEvent(new Event("input"));
    fontFamilyElement.dispatchEvent(new Event("input"));
    colorElement.dispatchEvent(new Event("input"));
    speedElement.dispatchEvent(new Event("input"));
});

function typeWriter() {
    $("#final").append("<p id='finaltxt'><p>");
    $("#final").append("<br>");
    $("#finaltxt").css({"font-size" : `${fontSize} px`, "font-family" : `${fontFamily}`,"color" : `${color}`});
    if (i < textValue.length) {
        $("#finaltxt").append(textValue.charAt(i));
        i++;
        setTimeout(typeWriter, speed);
    }
}

// Faltou adicionar um trigger para enviar quando é clicado o input submit com o id submitButton
function reset()
{
    i = 0;
    typeWriter();
}

$("#submitButton").on("click", reset());
